package me.enchanted.factions;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagDouble;
import net.minecraft.server.v1_8_R3.NBTTagInt;
import net.minecraft.server.v1_8_R3.NBTTagList;
import net.minecraft.server.v1_8_R3.NBTTagString;

public class Crafts {
	
	public static ShapedRecipe lightApple;
	public static ShapedRecipe nectar;
	public static ShapedRecipe dragonSword;
	public static ShapedRecipe enlighteningPack;
	public static ShapedRecipe goldenHead;
	public static ShapedRecipe flamingArtifact;
	public static ShapedRecipe ironPack;
	public static ShapedRecipe goldPack;
	
	public void register() {
		LA();
		NEC();
		DRAGS();
		ENLIGHT();
		GHEAD();
		FLART();
		IRONP();
		GOLDP();
	}
	
	public void LA() {
		ItemStack i = new ItemStack(Material.GOLDEN_APPLE, 1);
		
		 lightApple = new ShapedRecipe(i);
		
		lightApple.shape(" G ",
				     "GAG",
				     " G ");
		
		lightApple.setIngredient('G', Material.GOLD_INGOT);
		lightApple.setIngredient('A', Material.APPLE);
		
		Bukkit.addRecipe(lightApple);
	}
	
	public void NEC() {
		ArrayList<String> ar = new ArrayList<String>();
		ar.add("�7Regeneration III (0:10)");
		ar.add("�7Unbreaking I");
		
		ItemStack p = new ItemStack(Material.POTION);
		PotionMeta pm = (PotionMeta) p.getItemMeta();
		pm.setLore(ar);
		pm.setDisplayName("�aNectar");
		pm.addEnchant(Enchantment.DURABILITY, 1, true);
		pm.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		pm.addCustomEffect(new PotionEffect(PotionEffectType.REGENERATION, 10 * 20, 2),true);
		p.setItemMeta(pm);
		
		nectar = new ShapedRecipe(p);
		
		nectar.shape(" E ",
				         "GMG",
				         " B ");
		
		nectar.setIngredient('G', Material.GOLD_INGOT);
		nectar.setIngredient('E', Material.EMERALD);
		nectar.setIngredient('M', Material.MELON);
		nectar.setIngredient('B', Material.GLASS_BOTTLE);
		
		Bukkit.addRecipe(nectar);
	}
	
	public void DRAGS() {
		ItemStack i = new ItemStack(Material.DIAMOND_SWORD, 1);
		ItemMeta im = i.getItemMeta();
		
		
		im.setDisplayName("�aDragon Sword");
		
		i.setItemMeta(im);
		
		i = changeItemDamage(i, 8);
        
		dragonSword = new ShapedRecipe(i);
		
		dragonSword.shape(" B ",
				               " D ",
				               "OBO");
		
		dragonSword.setIngredient('D', Material.DIAMOND_SWORD);
		dragonSword.setIngredient('B', Material.BLAZE_POWDER);
		dragonSword.setIngredient('O', Material.OBSIDIAN);
		
		Bukkit.addRecipe(dragonSword);
	}
	
	public void ENLIGHT() {
		ItemStack i = new ItemStack(Material.EXP_BOTTLE, 8);
		
		enlighteningPack = new ShapedRecipe(i);
		
		enlighteningPack.shape(" R ",
				               "RBR",
				               " R ");
		
		enlighteningPack.setIngredient('R', Material.REDSTONE_BLOCK);
		enlighteningPack.setIngredient('B', Material.GLASS_BOTTLE);
		
		Bukkit.addRecipe(enlighteningPack);
	}
	
	@SuppressWarnings("deprecation")
	public void GHEAD() {
		ItemStack i = new ItemStack(Material.SKULL_ITEM, 1, (byte) 3);
		i = setMeta(i, "GoldenHead");
		SkullMeta im = (SkullMeta) i.getItemMeta();
		
		im.setOwner("0000000000000P");
		im.setDisplayName("�6Golden Head");
		
		i.setItemMeta(im);
		
		
		
		goldenHead = new ShapedRecipe(i);
		
		goldenHead.shape("GGG",
				         "GHG",
				         "GGG");
		
		goldenHead.setIngredient('G', Material.GOLD_INGOT);
		goldenHead.setIngredient('H', Material.SKULL_ITEM, 3);
		
		Bukkit.addRecipe(goldenHead);
	}
	
	@SuppressWarnings("deprecation")
	public void FLART() {
		ItemStack i = new ItemStack(Material.BLAZE_ROD, 1);
		//test
		flamingArtifact = new ShapedRecipe(i);
		
		flamingArtifact.shape("GLG",
				         "GFG",
				         "GLG");
		
		flamingArtifact.setIngredient('G', Material.STAINED_GLASS, 1);
		flamingArtifact.setIngredient('L', Material.LAVA_BUCKET);
		flamingArtifact.setIngredient('F', Material.FIREWORK);
		
		Bukkit.addRecipe(flamingArtifact);
	}
	
	public void IRONP() {
		ItemStack i = new ItemStack(Material.IRON_INGOT, 10);
		
		ironPack = new ShapedRecipe(i);
		
		ironPack.shape("III",
				         "ICI",
				         "III");
		
		ironPack.setIngredient('I', Material.IRON_ORE);
		ironPack.setIngredient('C', Material.COAL);
		
		Bukkit.addRecipe(ironPack);
	}
	
	public void GOLDP() {
		ItemStack i = new ItemStack(Material.GOLD_INGOT, 10);
		
		goldPack = new ShapedRecipe(i);
		
		goldPack.shape("GGG",
				         "GCG",
				         "GGG");
		
		goldPack.setIngredient('G', Material.GOLD_ORE);
		goldPack.setIngredient('C', Material.COAL);
		
		Bukkit.addRecipe(goldPack);
	}
	
	public ItemStack setMeta(ItemStack i, String value) {
		
		net.minecraft.server.v1_8_R3.ItemStack Itemstack = CraftItemStack.asNMSCopy(i);
		
		NBTTagCompound compound = new NBTTagCompound();
		
		compound.setString("UHC", value);
		
		Itemstack.setTag(compound);
		
		i = CraftItemStack.asBukkitCopy(Itemstack);
		
		return i;
	}
	
	public ItemStack changeItemDamage(ItemStack i, double damage) { 
		net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(i);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        
        NBTTagList modifiers = new NBTTagList();
       
        // -- attack damage
        NBTTagCompound attrib = new NBTTagCompound();
        attrib.set("AttributeName", new NBTTagString("generic.attackDamage"));
        attrib.set("Name", new NBTTagString("generic.attackDamage"));
        attrib.set("Amount", new NBTTagDouble(damage));
        attrib.set("Operation", new NBTTagInt(0));
        attrib.set("UUIDLeast", new NBTTagInt(894654));
        attrib.set("UUIDMost", new NBTTagInt(2872));
        attrib.set("Slot", new NBTTagString("mainhand"));
		
        modifiers.add(attrib);
        compound.set("AttributeModifiers", modifiers);
        
        nmsStack.setTag(compound);
        return CraftItemStack.asBukkitCopy(nmsStack);
	}
}
