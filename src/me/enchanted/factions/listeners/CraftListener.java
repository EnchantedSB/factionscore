package me.enchanted.factions.listeners;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.enchanted.factions.Crafts;
import net.minecraft.server.v1_8_R3.NBTBase;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagString;

public class CraftListener implements Listener {

	@EventHandler
	public void onCraft(PrepareItemCraftEvent e) {
		Player p = (Player) e.getView().getPlayer();
		
		if (e.getRecipe().equals(Crafts.lightApple)) {
			if (!p.hasPermission("crafts.lightapple")) {
				e.getInventory().setResult(null);
			}
		} else if (e.getRecipe().equals(Crafts.nectar)) {
			if (!p.hasPermission("crafts.nectar")) {
				e.getInventory().setResult(null);
			}
		} else if (e.getRecipe().equals(Crafts.dragonSword)) {
			if (!p.hasPermission("crafts.dragonsword")) {
				e.getInventory().setResult(null);
			}
		} else if (e.getRecipe().equals(Crafts.enlighteningPack)) {
			if (!p.hasPermission("crafts.enlighteningpack")) {
				e.getInventory().setResult(null);
			}
		} else if (e.getRecipe().equals(Crafts.goldenHead)) {
			if (!p.hasPermission("crafts.goldenhead")) {
				e.getInventory().setResult(null);
			}
		} else if (e.getRecipe().equals(Crafts.flamingArtifact)) {
			if (!p.hasPermission("crafts.flamingartifact")) {
				e.getInventory().setResult(null);
			}
		} else if (e.getRecipe().equals(Crafts.goldPack)) {
			if (!p.hasPermission("crafts.goldpack")) {
				e.getInventory().setResult(null);
			}
		} else if (e.getRecipe().equals(Crafts.ironPack)) {
			if (!p.hasPermission("crafts.ironpack")) {
				e.getInventory().setResult(null);
			}
		}
	}
	
	@EventHandler
	public void onGHead(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		
		if(e.getItem() == null || !e.getItem().hasItemMeta() || !e.getItem().getItemMeta().hasDisplayName()) return;
		
		if(getMetaUHC(e.getItem()).equalsIgnoreCase("GoldenHead")) {
			p.removePotionEffect(PotionEffectType.ABSORPTION);
			p.removePotionEffect(PotionEffectType.REGENERATION);
			p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 2*20*60, 0));
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 5*20, 1));
			if(e.getItem().getAmount() == 1) {
				e.getPlayer().setItemInHand(new ItemStack(Material.AIR));
			} else {
				e.getPlayer().getItemInHand().setAmount(e.getPlayer().getItemInHand().getAmount()-1);
			}
		}
	}
	
	public String getMetaUHC(ItemStack i) {
		net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);
		
		if(item.getTag() == null) {
			return "null";
		}
		
		return item.getTag().getString("UHC");
	}
}
