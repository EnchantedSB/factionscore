package me.enchanted.factions;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.enchanted.factions.listeners.CraftListener;

public class FactionsCore extends JavaPlugin implements Listener{
	
	private Crafts c = new Crafts();

	public void onEnable() {
		
		getServer().getPluginManager().registerEvents(new CraftListener(), this);
		
		c.register();
		
	}
	
	public void onDisable() {
		Bukkit.getScheduler().cancelAllTasks();
	}
}
